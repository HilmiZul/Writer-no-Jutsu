import processing.sound.*;

Mouse tikus;
Keju keju;
Senteg[] senteg = new Senteg[5];

PImage tikusUp, tikusRight, tikusDown, tikusLeft, bg, cheese,
       sentegImg;
float stamina = 0;
int skor;
PFont font;
boolean gameOver = false;

SoundFile bgMusik, eat, hit;

void setup() {
  size(600, 600);
  tikus = new Mouse();
  keju = new Keju();
  
  for(int i=0; i<senteg.length; i++) {
    senteg[i] = new Senteg();
  }
  
  tikusUp = loadImage("tikusUp.png");
  tikusRight = loadImage("tikusRight.png");
  tikusDown = loadImage("tikusDown.png");
  tikusLeft = loadImage("tikusLeft.png");
  cheese = loadImage("keju.png");
  sentegImg = loadImage("senteg4.png");
  
  bg = loadImage("rumput.png");
  
  font = createFont("Prototype", 50);
  
  bgMusik = new SoundFile(this, "bgMusik.wav");
  hit = new SoundFile(this, "hit.wav");
  eat = new SoundFile(this, "eat.mp3");
  bgMusik.play();
}

void draw() {
  //background(50, 200, 0);
  background(bg);
  
  tikus.show();
  tikus.move();
  tikus.cekTepi();
  tikus.eat(keju);
  
  for(int i=0; i<senteg.length; i++) {
    senteg[i].hiden();
    tikus.hit(senteg[i]);
  }
  
  keju.show();
  
  fill(255);
  textSize(35);
  textFont(font);
  text(skor, 32, 50);
}

void keyPressed() {
  if(keyCode == UP) {
    tikus.moveUp = true;
    tikus.moveRight = false;
    tikus.moveDown = false;
    tikus.moveLeft = false;
  } else if(keyCode == RIGHT) {
    tikus.moveUp = false;
    tikus.moveRight = true;
    tikus.moveDown = false;
    tikus.moveLeft = false;
  } else if(keyCode == DOWN) {
    tikus.moveUp = false;
    tikus.moveRight = false;
    tikus.moveDown = true;
    tikus.moveLeft = false;
  } else if(keyCode == LEFT) {
    tikus.moveUp = false;
    tikus.moveRight = false;
    tikus.moveDown = false;
    tikus.moveLeft = true;
  }
}